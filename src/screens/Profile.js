import React, { Component } from 'react';
import { StyleSheet, Button } from 'react-native';
import { Container, Content, Text } from 'native-base';

export default class Profile extends Component {

    static navigationOptions = ({navigation}) => ({
        title: `Profile ${navigation.state.params.name}`
    })
    render() {
        return (
            <Container style={styles.container}>
                <Content>
                    <Text style={styles.text}>Welcome To This App</Text>
                    <Button title="To Chat" onPress={() => this.props.navigation.navigate('Chat',{name: "Mughni"})}/>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        alignItems: "center"
    },
    text: {
        textAlign: "center",
        justifyContent: "flex-start"
    }
})