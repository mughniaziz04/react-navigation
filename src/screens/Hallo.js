import React, { Component } from 'react';
import { StyleSheet, Button } from 'react-native';
import { Container, Content, Text } from 'native-base';

export default class Hallo extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: `Chat With ${navigation.state.params.name}`
    })
    render() {
        return (
            <Container style={styles.container}>
                <Content>
                    <Text style={styles.text}>Chat With People</Text>
                    <Button title="To Home" onPress={() => this.props.navigation.navigate("Home")}/>
                    <Button title="Set Profile" onPress={() => this.props.navigation.navigate("Uprofile",{name: "Mughni"})}/>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        alignItems: "center"
    },
    text: {
        textAlign: "center",
        justifyContent: "flex-start"
    }
})