/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
// import { Container, Content, Text, Header} from 'native-base';
import { createStackNavigator, createAppContainer} from 'react-navigation';
import Welcome from './src/screens/Welcome';
import Hallo from './src/screens/Hallo';
import Profile from './src/screens/Profile';


const App = createStackNavigator({
  Home: { screen: Welcome},
  Chat: { screen: Hallo},
  Uprofile: { screen: Profile}
})
export default createAppContainer(App)

